﻿using System;

// Lucas Pereira Zaranza

namespace CaixaEletronico
{
    internal class Caixa
    {
        internal string EntregarCedulas(int valor)
        {
            if (valor < 10)
                return "Impossivel sacar valor menor que 10 reais.";
            else if(valor % 10 != 0)
                return "Impossivel sacar valor nao multiplo de 10.";
            else
                return DecomporValor(valor);           
        }

        internal string DecomporValor(int valor)
        {
            string result = "Entregar";
            int valorAtual = valor;
            int notasDeDez = 0;       int notasDeVinte = 0;
            int notasDeCnqt = 0;      int notasDeCem = 0;

            while(valorAtual > 0)
            {
                if (valorAtual == 10)
                {                    
                    notasDeDez = valorAtual / 10;
                    valorAtual %= 10;
                }
                else if (valorAtual < 50)
                {                    
                    notasDeVinte = valorAtual / 20;
                    valorAtual %= 20;
                }
                else if(valorAtual < 100)
                {
                    notasDeCnqt = valorAtual / 50;
                    valorAtual %= 50;
                }
                else
                {
                    notasDeCem = valorAtual / 100;
                    valorAtual %= 100;
                }       
            }

            if(notasDeCem > 0)
                result += string.Format(" {0} nota de 100", notasDeCem);
            if (notasDeCnqt > 0)
                result += string.Format(" {0} nota de 50", notasDeCnqt);
            if (notasDeVinte > 0)
                result += string.Format(" {0} nota de 20", notasDeVinte);
            if (notasDeDez > 0)
                result += string.Format(" {0} nota de 10", notasDeDez);

            result += '.';
            return result;
        }
    }
}