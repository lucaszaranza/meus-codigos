﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// Lucas Pereira Zaranza

namespace CaixaEletronico
{
    [TestClass]
    public class UnitTest1
    {        
        Caixa caixaEletronico;

        [TestInitialize]
        public void Setup()
        { 
            caixaEletronico = new Caixa();            
        }

        [TestMethod]
        public void ClasseFoiInstanciada()
        {
            Assert.IsNotNull(caixaEletronico);
        }

        [TestMethod]
        public void ValorMenorque10RetornaErro()
        {
            string resultado = caixaEletronico.EntregarCedulas(0);
            Assert.AreEqual("Impossivel sacar valor menor que 10 reais.", resultado);
        }

        [TestMethod]
        public void Entrega10Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(10);
            Assert.AreEqual("Entregar 1 nota de 10.", resultado);
        }

        [TestMethod]
        public void Entrega20Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(20);
            Assert.AreEqual("Entregar 1 nota de 20.", resultado);
        }

        [TestMethod]
        public void Entrega50Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(50);
            Assert.AreEqual("Entregar 1 nota de 50.", resultado);
        }

        [TestMethod]
        public void Entrega100Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(100);
            Assert.AreEqual("Entregar 1 nota de 100.", resultado);
        }

        [TestMethod]
        public void ValorQuebradoRetornaErro()
        {
            string resultado = caixaEletronico.EntregarCedulas(51);
            Assert.AreEqual("Impossivel sacar valor nao multiplo de 10.", resultado);
        }

        [TestMethod]
        public void Entrega30Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(30);
            Assert.AreEqual("Entregar 1 nota de 20 1 nota de 10.", resultado);
        }

        [TestMethod]
        public void Entrega40Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(40);
            Assert.AreEqual("Entregar 2 nota de 20.", resultado);
        }

        [TestMethod]
        public void Entrega60Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(60);
            Assert.AreEqual("Entregar 1 nota de 50 1 nota de 10.", resultado);
        }

        [TestMethod]
        public void Entrega70Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(70);
            Assert.AreEqual("Entregar 1 nota de 50 1 nota de 20.", resultado);
        }

        [TestMethod]
        public void Entrega80Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(80);
            Assert.AreEqual("Entregar 1 nota de 50 1 nota de 20 1 nota de 10.", resultado);
        }

        [TestMethod]
        public void Entrega90Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(90);
            Assert.AreEqual("Entregar 1 nota de 50 2 nota de 20.", resultado);
        }

        [TestMethod]
        public void Entrega110Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(110);
            Assert.AreEqual("Entregar 1 nota de 100 1 nota de 10.", resultado);
        }

        [TestMethod]
        public void Entrega170Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(170);
            Assert.AreEqual("Entregar 1 nota de 100 1 nota de 50 1 nota de 20.", resultado);
        }

        [TestMethod]
        public void Entrega1000Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(1000);
            Assert.AreEqual("Entregar 10 nota de 100.", resultado);
        }

        [TestMethod]
        public void Entrega1280Reais()
        {
            string resultado = caixaEletronico.EntregarCedulas(1280);
            Assert.AreEqual("Entregar 12 nota de 100 1 nota de 50 1 nota de 20 1 nota de 10.", resultado);
        }
    }
}